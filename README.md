This is solution for LeetCode taks:
https://leetcode.com/problems/same-tree/

I prefer recursion for working with trees and graphs.

Alghoritm of function:
![Alghoritm](.gitignore/alghoritm.png)

Result test:
![Result test](.gitignore/result.PNG)

Big O notation:
Time: 

O(n) = n // When we need to check all nodes

Memory : Call stack for recursion

O(n) = n //When we need to check all nodes