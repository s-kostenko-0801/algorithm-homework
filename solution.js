/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */

//If value is equal, checking leaves value, else return false
var isSameTree = function(p, q) {
    //Null checking block
    if(!p||!q){
        //If both are null
        if(p===q)
            return true;
    }
    //Value checking block
    else
        if(p.val==q.val){
            //Recursion 
            return isSameTree(p.left,q.left)&&isSameTree(p.right,q.right);
        }
    return false;
    
};